---
layout: job_family_page
title: "Internal Auditor"
---

The Internal Auditor is responsible for planning and execution of audits in accordance with the annual audit plan, monitoring the status of implementation of audit recommendations, and helping with any advisory or ad hoc work as desired by management. The Internal Auditor brings a systematic, disciplined approach to the assessment and improvement of governance, risk management, and control process.

## Senior Internal Auditor

### Responsibilities
- Contributes to the development of the annual audit plan for business units in the assigned audit portfolio
- Develops detailed and thorough risk assessment for each audit engagement
- Owns each assigned audit from the initial planning to completion
- Effectively communicates the objectives, scope and timelines of the audit to the auditees and relevant stakeholders at the start of the engagement
- Documents detailed working papers and ensures that audit conclusions are supported by sufficient and relevant audit evidence
- Effectively communicates audit findings to the auditee
- Drafts audit reports including value-added observations
- Follows up and monitors the progress of the implementation of recommendations
- Develops and maintains effective working relationships with business and corporate functions
- Supports the alignment of activities between the control functions (i.e. Compliance, Finance, External Auditors, etc.) to improve communications and efficiency of audit and risk management activities
- Assists with quarterly and annual reporting to the Audit Committee, Board of Directors and Senior Management
- Performs advisory engagements, investigations and special projects as assigned
- Continually improves skills and competencies required for the position
- Identifies opportunities for internal audit to provide value added services to the organization
- Maintains an up-to-date knowledge of the standards and guidance included in the International Professional Practice Framework (IPPF) developed by the IIA; stays current with evolving knowledge in the field of Internal Auditing; maintains compliance with IIA standards and its Code of Ethics

### Requirements

- Minimum five (5) years experience conducting internal audits, including minimum two (2) years of experience in Information Technology industry in internal / external audit preferred. 
- Audit experience at a public accounting firm is considered as an asset
- Knowledge of audit methodologies and frameworks and related governance concepts, tools, techniques, and best practices
- Undergraduate degree in Business, Accounting or Finance
- Chartered Accountant/ CPA /CIA certification preferred
- Certified Internal Auditor (CIA), Certified Information Systems Auditor (CISA), or Certified Fraud Examiner (CFE) designations are considered an asset
- Excellent written and oral communication skills
- Have an understanding of enterprise risk management framework such as COSO enterprise risk management framework
- Ability to communicate information in an understandable form to the right parts of the organization
- Ability to work effectively in a team environment, both within Internal Audit and across other departments
- Must be able to work in US and Canada timezones, when required

## Performance Indicators
- [Percentage of Desktop procedures documented](/handbook/internal-audit/#internal-audit-performance-measures) 
- [Percentage of controls tested](/handbook/internal-audit/#internal-audit-performance-measures)  
- [Percentage of recommendations implemented](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of audits completed](/handbook/internal-audit/#internal-audit-performance-measures) 


### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

1. Selected candidates will be invited to schedule a screening call with our Global Recruiters
1. Next, candidates will be invited to schedule a first interview with our Senior Internal Audit Manager
1. Candidates will then be invited to schedule a second round of interview with Principal Accounting Officer
1. Candidates will then be invited to schedule a final round of interview with our CFO.

Additional details about our process can be found on our hiring page.
